<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        session_start();
        if(!isset($_SESSION["login"])){
            $login = strtoupper($_POST["login"]);
            $pwd = strtoupper($_POST["pwd"]);

            if($login == "WEB" && $pwd == "MASTER"){
                $_SESSION["login"] = true;
                header("location: ./login.php");
            }else{
                $_SESSION["rate"] = true;
                header("location: ./credentials.php");
            }
        }else if (!isset($_SESSION["film"])){
            $_SESSION["film"] = true;
            header("location: ./login.php");
        }

        if(isset($_SESSION["logout"])){
            session_destroy();
            header("location: ./login.php");
        }
        ?>
    </body>
</html>
