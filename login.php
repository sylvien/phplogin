<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./loginStyle.css">
    </head>
    <body>
        <nav>
            <div class="one">                
                <?php
                session_start();
                if(!isset($_SESSION["login"])){
                    echo '<a href="./credentials.php">LOGIN</a>';
                }else if(isset($_SESSION["login"]) && !isset($_SESSION["film"])){
                    echo '<a href="./loginTraitement.php">LOGOUT</a>';
                    echo '<a href="./loginTraitement.php">CONTENU</a>';
                } else{
                    echo '<a href="./loginTraitement.php">LOGOUT</a>';
                    echo '<a href="./login.php">CONTENU</a>';
                    $_SESSION["logout"] = true;
                }
                ?>
            </div>

        </nav>
        <?php
            if(isset($_SESSION["film"])){
                $film = ["film1", "film2", "film3"];
                echo '<div style="margin-top: 25vh">';
                foreach ($film as $url) {
                    echo '<img src="./' . $url . '.jpg"><br>';
                }
                echo '</div>';
            }
        ?>
    </body>
</html>